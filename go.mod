module apiote.xyz/p/gonk

go 1.18

require (
	gitea.com/lunny/html2md v0.0.0-20181018071239-7d234de44546
	golang.org/x/net v0.0.0-20220607020251-c690dde0001d
)
